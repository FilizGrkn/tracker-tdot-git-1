import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
import argparse
import coco

import shutil

import time

from mrcnn import utils
# import mrcnn.model as modellib
# import mrcnn.model as modellib
import mrcnn.model_case1 as modellib
import mrcnn.visualize
from mrcnn.config import Config

# from kcf.kcf_particle_full_version import KCF
from kcf.kcf_particle_full_version_1 import KCF
import cv2 

from lbp import lbp_decimal_8
from lbp import chi_square_dist
from lbp import L2_dist
from lbp import resize_bbox

## region [Parse Args] --
# Test some videos
parser = argparse.ArgumentParser(description='Test some videos.')

# parser.add_argument('--batch-mode',
#                     action='store_true',
#                     default=False,
#                     help='States that we are working in batch mode')

parser.add_argument('--overwrite-output',
                    action='store_true',
                    default=False,
                    help='Whether we should overwrite the output files if they exist')

parser.add_argument("--mode", required=True,
                    metavar="<mode>",
                    help="Indicate the model mode as 'inference',"
                         "'extension' or 'inference_rpn'.")
parser.add_argument('--test-dataset-dir', metavar='TD', type=str,
                    default=None,
                    help='enter the test directory')
parser.add_argument('--model-dir', metavar='MD', type=str,
                    default=None,
                    help='enter the model file path')

parser.add_argument('--out-dir', metavar='OD', type=str,
                    default=None,
                    help='folder directory for saving results')
# parser.add_argument('--target-class-name', metavar='TCN', type=str,
#                     default=None,
#                     help='class of tracked object')

parser.add_argument('--target-class-name-file', metavar='TCNF', type=str,
                    default=None,
                    help='class of tracked object')

# parser.add_argument('--initial-target-frame', metavar='ITF', type=float,
#                     nargs=4,
#                     default=None,
#                     help='Initial bbox for the object to track: [x,y,w,h]')
parser.add_argument('--ground-truth-folder', metavar='GTF', type=str,
                    default=None,
                    help='GT folder to read initial bboxes for the objects to track: [x,y,w,h]')


args = parser.parse_args()

# Local path to trained weights file
ARG_COCO_MODEL_PATH = args.model_dir

# Directory of images to run detection on
ARG_IMAGE_DIR = os.path.join(args.test_dataset_dir)

# ARG_TARGET_CLASS_NAME = args.target_class_name
# Full path to the target_class_name_file
ARG_TARGET_CLASS_NAME_FILE = args.target_class_name_file

ARG_MODE = args.mode

ARG_OUT_DIR = args.out_dir

# ARG_INITIAL_TARGET_FRAME = args.initial_target_frame
ARG_GROUND_TRUTH_FOLDER = args.ground_truth_folder

# BATCH_MODE = args.batch_mode

ARG_OVERWRITE_OUTPUT = args.overwrite_output
## endregion [Parse Args] --

## region [Initialization & Validation] --

# Model Path

# region [COCO Model File Check] --
assert ARG_COCO_MODEL_PATH is not None
# Download COCO trained weights from Releases if needed
if not os.path.exists(ARG_COCO_MODEL_PATH):
    utils.download_trained_weights(ARG_COCO_MODEL_PATH)
# endregion [COCO Model File Check] --

# region [COCO class name init & target validation] --
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']

# if ARG_TARGET_CLASS_NAME is not None:
#     assert ARG_TARGET_CLASS_NAME in class_names,"Target class name is not included in class_names list!"
target_class_names_dict = {}
# print("type: ",type(ARG_TARGET_CLASS_NAME_FILE))

#print("\n\nFrom script: ")
#print(ARG_TARGET_CLASS_NAME_FILE)
#print("\n",ARG_IMAGE_DIR)

#ARG_TARGET_CLASS_NAME_FILE='C:\\Users\\eksamin4\\Desktop\\PhD\\Research\\Correlation_Filter_based_Tracking\\Code\\TDOT\\target_class_file.txt'
#ARG_IMAGE_DIR = "'C:\\Users\\eksamin4\\Desktop\\PhD\\Research\\Correlation_Filter_based_Tracking\\Code\\TDOT\\data\\'"

#print("\nManually: ")
#print(ARG_TARGET_CLASS_NAME_FILE)
#print("\n",ARG_IMAGE_DIR)

if ARG_TARGET_CLASS_NAME_FILE is not None:

    assert os.path.exists(ARG_TARGET_CLASS_NAME_FILE), "Target class name file does not exist on path: {}".format(ARG_TARGET_CLASS_NAME_FILE)
    with open(ARG_TARGET_CLASS_NAME_FILE,'r') as f:
        for line in f:
            tokens = line.split('=')
            tokens[1] = tokens[1].replace('\n','')
            assert tokens[1] in class_names, "Target class name '{}' is not included for video '{}'".format(tokens[1],tokens[0])
            target_class_names_dict[tokens[0]] = tokens[1]
# endregion [COCO class name init & target validation] --

PARTICLE_COUNT = 200


# region [Validation of directories] --

# Root directory of the project
ROOT_DIR = os.getcwd()

particle_file_names = []

video_directories = []
video_names = []
frame_folder_names = sorted(os.listdir(ARG_IMAGE_DIR))
for folder_name in frame_folder_names:
    # Ignore items in the data folder which are not directories
    if os.path.isdir(os.path.join(ARG_IMAGE_DIR, folder_name)):
        video_names.append(folder_name)
        video_directories.append(os.path.join(ARG_IMAGE_DIR, folder_name))

if not os.path.exists(ARG_OUT_DIR):
    os.makedirs(ARG_OUT_DIR)

def construct_ground_truth_filename(video_name):
    return ARG_GROUND_TRUTH_FOLDER.strip() + "/"+video_name.strip() + "/groundtruth.txt"

if ARG_GROUND_TRUTH_FOLDER is not None:
    for video_id, video_dir in enumerate(video_directories):
        gt_filename = construct_ground_truth_filename(video_names[video_id])
        assert os.path.exists(gt_filename), "Ground truth file does not exist: {}".format(gt_filename)

# endregion [Validation of directories] --

# region [Output file override validation] --
# check output file names, and warn if any output will be overrided beforehand
HEAD_OUTPUT_SUFFIX = "_head_output"
PARTICLE_OUTPUT_SUFFIX = "_particles.txt"
RPN_OUTPUT_SUFFIX = "_rpn"
MASK_OUTPUT_SUFFIX = "_mask"

def construct_head_output_filename(video_name):
    return ARG_OUT_DIR + "/" + video_name + HEAD_OUTPUT_SUFFIX
def construct_selected_head_output_filename(video_name):
    return ARG_OUT_DIR + "/" + video_name + HEAD_OUTPUT_SUFFIX + "_selected"
def construct_particle_output_filename(video_name):
    return ARG_OUT_DIR + "/" + video_name + PARTICLE_OUTPUT_SUFFIX
def construct_rpn_output_filename(video_name):
    return ARG_OUT_DIR + "/" + video_name + RPN_OUTPUT_SUFFIX
def construct_mask_output_foldername(video_name):
    return ARG_OUT_DIR + "/" + video_name + MASK_OUTPUT_SUFFIX + "/"

def conditionally_remove_file(file_path):
    if os.path.exists(file_path):
        if ARG_OVERWRITE_OUTPUT:
            os.remove(file_path)
        else:
            raise AssertionError("File exists: {}".format(file_path))

def conditionally_remove_folder(folder_path):
    if os.path.exists(folder_path) and os.path.isdir(folder_path):
        if ARG_OVERWRITE_OUTPUT:
            shutil.rmtree(folder_path, ignore_errors=True)
        else:
            raise AssertionError("Folder exists: {}".format(folder_path))

for video_id, video_dir in enumerate(video_directories):
    video_name = video_names[video_id]
    if ARG_MODE == "rpn":
        rpn_file_path = construct_rpn_output_filename(video_name)
        conditionally_remove_file(rpn_file_path)
    else:
        mask_file_path = construct_head_output_filename(video_name)
        conditionally_remove_file(mask_file_path)
        selected_mask_file_path = construct_selected_head_output_filename(video_name)
        conditionally_remove_file(selected_mask_file_path)
        particle_file_path = construct_particle_output_filename(video_name)
        conditionally_remove_file(particle_file_path)
        mask_folder_path = construct_mask_output_foldername(video_name)
        conditionally_remove_folder(mask_folder_path)
        if not os.path.exists(mask_folder_path):
            os.makedirs(mask_folder_path)

# endregion [Output file override validation] --

## endregion [Initialization & Validation] --

# region [Keras & Mask-RCNN Initialization] --

import keras.backend as K
import tensorflow as tf

gpu_options = tf.GPUOptions(allow_growth=True)
# gpu_options = tf.GPUOptions(allocator_type = 'BFC')
config_keras = tf.ConfigProto(gpu_options=gpu_options)
K.set_session(tf.Session(config=config_keras))

class InferenceConfig(Config):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    global ARG_MODE
    NAME = "coco_evaluation"
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    # NUM_CLASSES = 1 + 1
    NUM_CLASSES = 80 + 1
    DETECTION_MIN_CONFIDENCE = 0.0
    DETECTION_NMS_THRESHOLD = 0.7
    FILTER_BACKGROUND = True

    if ARG_MODE == "extension" or "rpn":
        POST_PS_ROIS_INFERENCE = PARTICLE_COUNT
        DETECTION_MAX_INSTANCES = 400
        # POST_PS_ROIS_INFERENCE = 1000
        # DETECTION_MAX_INSTANCES = 1000
    elif ARG_MODE == "inference":
        POST_PS_ROIS_INFERENCE = 1000
        DETECTION_MAX_INSTANCES = 1000

    INIT_BN_BACKBONE = True
    INIT_GN_BACKBONE = False
    INIT_BN_HEAD = True
    INIT_GN_HEAD = False

    # USE_BOTTOM_UP_AUG = False # Bottom-up augumentation setting
    # LATERAL_SHORTCUTS = False # Green and red dash connections
    # FC_MASK_FUSION = False # Mask fusion setting


config = InferenceConfig()
config.display()

# Create model object in inference mode.
model = modellib.MaskRCNN(mode=ARG_MODE, model_dir=ARG_COCO_MODEL_PATH, config=config)

# Load weights trained on MS-COCO
print("Loading weights")
model.load_weights(ARG_COCO_MODEL_PATH, by_name=True)

# endregion [Keras & Mask-RCNN Initialization] --

# region [Utility Functions] --
def coco_to_voc_bbox_converter(y1, x1, y2, x2, roi_score=-1):
    w = x2 - x1
    h = y2 - y1
    return x1, y1, w, h, roi_score



def to_rgb1(im):
    # I think this will be slow
    w, h = im.shape
    ret = np.empty((3, w, h), dtype=np.uint8)
    ret[0, :, :] = im
    ret[1, :, :] = im
    ret[2, :, :] = im
    return ret



# giris x,y,w,h cikis y1,x1,y2,x2 olmali
def particle_mask_normalize(particles, first_img, config=config):
    first_img = skimage.io.imread(first_img)
    dim_y, dim_x, _ = first_img.shape

    for i in range(0,particles.shape[0]):
        # print("particle [{}] before: {}".format(i,particles[i]))
        x, y, w, h = particles[i]
        particles[i, 0] = y / dim_y
        particles[i, 1] = x / dim_x
        particles[i, 2] = (y + h) / dim_y
        particles[i, 3] = (x + w) / dim_x
        # print("particle [{}] after: {}".format(i,particles[i]))

    particles_np = np.array(particles).astype(np.float64)



    # Get the scale and padding parameters by using resize_image.
    _, _, scale, pad, _ = utils.resize_image(first_img,
                                             min_dim=config.IMAGE_MIN_DIM,
                                             max_dim=config.IMAGE_MAX_DIM,
                                             min_scale=config.IMAGE_MIN_SCALE,
                                             mode="square")

    # Roughly calculate padding across different axises.
    aver_pad_y = (pad[0][0] + pad[0][1]) / 2
    aver_pad_x = (pad[1][0] + pad[1][1]) / 2

    particles_np *= np.array((dim_y, dim_x, dim_y, dim_x))
    particles_np = (particles_np * scale) + np.array((aver_pad_y, aver_pad_x, aver_pad_y, aver_pad_x))
    particles_np /= np.array(
        (1024, 1024, 1024, 1024))  ## ozgun soru: bunun config.IMAGE_MAX_DIM vs olması gerekmiyor muydu?
    particles_np = particles_np.reshape((-1, config.POST_PS_ROIS_INFERENCE, 4))
    return particles_np

class VotGt2016Bbox:
    #def __init__(self, top_left_x, top_left_y, top_right_x, top_right_y, bottom_right_x, bottom_right_y, bottom_left_x, bottom_left_y):
    def __init__(self, tokens):
        # self.top_left_x = top_left_x
        # self.top_left_y = top_left_y
        # self.top_right_x = top_right_x
        # self.top_right_y = top_right_y
        # self.bottom_right_x = bottom_right_x
        # self.bottom_right_y = bottom_right_y
        # self.bottom_left_x = bottom_left_x
        # self.bottom_left_y = bottom_left_y

        self.top_left_x = float(tokens[0])
        self.top_left_y = float(tokens[1])
        self.top_right_x = float(tokens[2])
        self.top_right_y = float(tokens[3])
        self.bottom_right_x = float(tokens[4])
        self.bottom_right_y = float(tokens[5])
        self.bottom_left_x = float(tokens[6])
        self.bottom_left_y = float(tokens[7])

    def convert_to_mrcnn_bbox(self):
        """
        (1)----(2)
        \       \
        \       \
        (4)----(3)

        From format
            x1,y1,x2,y2,x3,y3,x4,y4
        to format
            x1,y1,w,h
        :return:
        """
        left_avg = math.floor((self.top_left_x + self.bottom_left_x) / 2)
        top_avg = math.floor((self.top_left_y + self.top_right_y) / 2)
        width_avg = math.floor(((self.top_right_x - self.top_left_x)+(self.bottom_right_x - self.bottom_left_x))/2)
        height_avg = math.floor(((self.bottom_left_y - self.top_left_y) + (self.bottom_right_y - self.top_right_y)) / 2)
        return [left_avg, top_avg, width_avg, height_avg]

def are_corners_inimage(image, x,y,w,h):
    overflow = None
    row, col, d = image.shape

    if y<0:
        overflow  = 'U'
    elif y+h > row:
        overflow = 'D'

    elif x <0:
        overflow = 'L'
    elif x+w > col:
        overflow = 'R'

    return overflow


def crop_image(image, x,y,w,h):

    overflow = are_corners_inimage(image, x,y,w,h)
    
    if y<0:
        y = 0
    if x<0:
        x  = 0

    img = image[y:y+h, x:x+w, :]
    if img.size == 0:
        return None,overflow
    else:
        return img,overflow

def get_initial_ground_truth(video_name):
    with open(construct_ground_truth_filename(video_name),'r') as f:
        line = f.readline()
        tokens = line.split(',')
        tokens = list(filter(None, tokens))  # filter whitespace
        gt_bbox = VotGt2016Bbox(tokens).convert_to_mrcnn_bbox()
    return gt_bbox # x,y,w,h
# endregion [Utility Functions] --

# Start testifying images for every frame in a particular folder_name.
# When enumerator hits the batch size number, the model will begin detection.
video_counter = 0


# TARGET_CLASS_ID = class_names.index(ARG_TARGET_CLASS_NAME) if ARG_TARGET_CLASS_NAME is not None else -1
GAUSS_STDEV_COORD_INIT = 5
GAUSS_STDEV_SCALE_INIT = 5
GAUSS_STDEV_COORD = GAUSS_STDEV_COORD_INIT
GAUSS_STDEV_SCALE = GAUSS_STDEV_SCALE_INIT
STDEV_COORD_MISS_MULTIPLIER = 3  # NEW_GAUSS_STDEV_COORD = OLD_GAUSS_STDEV_COORD * STDEV_COORD_MISS_MULTIPLIER
STDEV_SCALE_MISS_MULTIPLIER = 1  # NEW_GAUSS_STDEV_SCALE = OLD_GGAUSS_STDEV_SCALE * STDEV_SCALE_MISS_MULTIPLIER

NO_RESULT_RESET_THRESHOLD = 2
# SAMPLE_MODE = utils.SampleMode.NO_MOTION
# SAMPLE_MODE = utils.SampleMode.MOTION

#ROI_THRESHOLD = 500
#roi_threshold = ROI_THRESHOLD

print("\n\nvideo_directories: ")

for video_id, video_dir in enumerate(video_directories,0):
    print(video_dir)

print("\n\n")

for video_id, video_dir in enumerate(video_directories,0):

    image_ids = os.listdir(video_dir)
    sorted_image_ids = sorted(image_ids, key=lambda x: x[:-4])
    sorted_image_ids = list(filter(lambda x: x[-4:] == ".jpg", sorted_image_ids))
    image = skimage.io.imread(os.path.join(video_dir, sorted_image_ids[0]))
    # print("LL type image: ", type(image))
    # print("LL shape",image.shape)
    if len(image.shape) == 2:
        image = to_rgb1(image)
    dims = image.shape
    canvas_shape_x_y_format = [dims[1], dims[0]]
    
    video_name = video_names[video_id]
    # Initialize particles
    if ARG_GROUND_TRUTH_FOLDER is None or ARG_MODE == "rpn":
        particles = np.zeros((1, PARTICLE_COUNT, 4))  # ilk framede particlelar hep 0 olacak
    else:
        particles = utils.sample_bbox(seed=get_initial_ground_truth(video_name), stdev_coord=GAUSS_STDEV_COORD,stdev_scale=GAUSS_STDEV_SCALE, count=PARTICLE_COUNT, canvas_shape=canvas_shape_x_y_format)
    
    print("Video in Process: {}/{}".format(video_id + 1, len(video_directories)))
    print("Video name: {}".format(video_name))
    image_list = []
    print(ARG_IMAGE_DIR)
    print("")
    print(video_dir)
    TARGET_CLASS_ID = class_names.index(
        target_class_names_dict[video_name]) if ARG_TARGET_CLASS_NAME_FILE is not None else -1
    print("Target class name: {}, id: {}".format(target_class_names_dict[video_name], TARGET_CLASS_ID))
    image_counter = 0

    no_result_counter = 0
    match_bbox_for_particle_seed_last = get_initial_ground_truth(video_name)



    #  Initializing Kernelized Correlation Filter (KCF)
    kcf_tracker = KCF(padding=1, features='color', interp_factor=0.00)
    kcf_tracker.init(image,match_bbox_for_particle_seed_last)

    bbox_lbp_reference, overflow_bbbox = crop_image(image,match_bbox_for_particle_seed_last[0],match_bbox_for_particle_seed_last[1],match_bbox_for_particle_seed_last[2],match_bbox_for_particle_seed_last[3])
    distance_lbp_list = []
    object_in_video = True
    lbp_threshold = 100
    lbp_threshold_factor = 3.0  #threshold for lbp distance =lbp_threshold_factor* lbp_threshold
    lbp_threshold_factor_nonfound = 4.0  #threshold for lbp distance =lbp_threshold_factor* lbp_threshold
 

    seed_list = []
    seed_list.append(match_bbox_for_particle_seed_last)
    MAX_SEED_COUNT = 2
    #  Her zaman frame-by-frame işleyecek, batch by batch işleyemiyor artık
    total_frame_count = len(sorted_image_ids)
    current_frame_index = 0
    # for d, image_id in enumerate(sorted_image_ids):


    while current_frame_index < total_frame_count:
        image_name = sorted_image_ids[current_frame_index]
        print(image_name)
        if current_frame_index == 0:
            current_frame_index += 1
            continue

        if (image_name[-4:] == ".jpg"):
            # print(skimage.io.imread(os.path.join(video_dir, image_id)))
            image = skimage.io.imread(os.path.join(video_dir, image_name))
            if len(image.shape) == 2:
                image = to_rgb1(image)
            image_list.append(image)
            dims = image.shape
            canvas_shape_x_y_format = [dims[1], dims[0]]
            if ARG_MODE == "rpn":
                # Get the scale and padding parameters by using resize_image.
                _, _, scale, pad, _ = utils.resize_image(image,
                                                         min_dim=config.IMAGE_MIN_DIM,
                                                         max_dim=config.IMAGE_MAX_DIM,
                                                         min_scale=config.IMAGE_MIN_SCALE,
                                                         mode="square")
                # Roughly calculate padding across different axises.
                aver_pad_y = (pad[0][0] + pad[0][1]) / 2
                aver_pad_x = (pad[1][0] + pad[1][1]) / 2

        
        print("Processed Frame ID: {}/{}".format(current_frame_index + 1, len(sorted_image_ids)))
        if ARG_MODE == "rpn":
            pass

        else:
            # region [FULL CYCLE BLOCK (Inference/Extension)] --
            results = model.detect(image_list, verbose=1, particles=particle_mask_normalize(particles[0],os.path.join(video_dir, os.listdir(video_dir)[0]),config)[0])
            r = results[0]
            image_list.clear()
            # feedback
            match_bbox_for_particle_seed = None
            closest_bbox_to_last = None
            selected_score_id = -1
            passed_highest_score_flag = False

            max_corr_score_mask = None
            min_corr_score_mask = None
            tdot_candidate_satisfied = False
            tdot_candidate_is = False

            
            b_box_image = np.array(image)
            for score_id, scores in enumerate(r['scores']):
                tdot_candidate_is= True
                # print("\nLL score_id: ", score_id,"\n")
                # region [Calculate Head Outputs] --
                y1, x1, y2, x2 = r['rois'][score_id]
                obj_score = r['scores'][score_id]
                predicted_class_id = r['class_ids'][score_id]

                x_part, y_part, w_part, h_part, score_part = coco_to_voc_bbox_converter(y1, x1, y2, x2, obj_score)
                bbox_candidate = x_part, y_part, w_part, h_part
     
                res_b_box = kcf_tracker.compute_correlation(image,np.array([[x_part, y_part, w_part, h_part]]))
                response_p_arr_pvpr = kcf_tracker.response_p_arr_pvpr
                response_p_arr_psr = kcf_tracker.response_p_arr_psr
                response_p_arr_epsr = kcf_tracker.response_p_arr_epsr


                b_box_image = cv2.rectangle(b_box_image, (x_part, y_part), (w_part+x_part, h_part+y_part), (0,0,255), 4)
              
                target_condition_satisfied = False
                roi_condition_satisfied = False
                closest_condition_satisfied = False

                if TARGET_CLASS_ID == -1: # not targeted
                    target_condition_satisfied = True
                elif predicted_class_id == TARGET_CLASS_ID:
                    target_condition_satisfied = True

                
                if max_corr_score_mask is None and target_condition_satisfied==True:
                    min_corr_score_mask = response_p_arr_psr[0]
                elif min_corr_score_mask is not None and response_p_arr_psr[0]< min_corr_score_mask and target_condition_satisfied:
                    min_corr_score_mask = response_p_arr_psr[0]


                if max_corr_score_mask is None and target_condition_satisfied==True:
                    max_corr_score_mask = response_p_arr_psr[0]
                    tdot_candidate_satisfied = True
                    bbox_final_tdot = bbox_candidate
                    
                    no_result_counter = 0
                    selected_score_id = score_id

                elif max_corr_score_mask is not None and response_p_arr_psr[0]> max_corr_score_mask and target_condition_satisfied:
                    print("\n ELIF")
                    max_corr_score_mask = response_p_arr_psr[0]
                    tdot_candidate_satisfied =  True

                    bbox_final_tdot = bbox_candidate

                    
                    no_result_counter = 0
                    selected_score_id = score_id


            if tdot_candidate_satisfied==True:
            # if max_corr_score_mask is not None and max_corr_score_mask>response_corr_psr:
                TDOT_CHOOSE = True
                
                 
                # unnucesary - just to write it in text file
                final_corr_score = max_corr_score_mask
                final_corr_score_min = min_corr_score_mask

                x,y,w,h = bbox_final_tdot
                # x_final_corr, y_final_corr, w_final_corr,h_final_corr = 
                print("\nLL TRUE final_corr_score (psr): ",final_corr_score)
                # to draw bbox !
                b_box_image = cv2.rectangle(b_box_image, (x, y), (x+w, y+h), (255,0,0), 1)

                # initialize correlation filter
                kcf_tracker.init(image,bbox_final_tdot)

                # set particle seed 
                match_bbox_for_particle_seed = [x,y,w,h]
                
                # crop bbox
                bbox_lbp_candidate, overflow_bbbox =  crop_image(image,x,y,w,h)
                
                # resize bbo to reference bounding box
                bbox_lbp_candidate_resize = resize_bbox(bbox_lbp_candidate,bbox_lbp_reference.shape[1],bbox_lbp_reference.shape[0])
                

                if bbox_lbp_candidate is not None:
                    lbp_hist_reference, bins = lbp_decimal_8(bbox_lbp_reference, hist_conc=True)
                    lbp_hist_condidate, bins = lbp_decimal_8(bbox_lbp_candidate_resize, hist_conc=True)

                    dist_chi_square_hist = chi_square_dist(lbp_hist_reference,lbp_hist_condidate)
                else:
                    dist_chi_square_hist = -1.0


                if dist_chi_square_hist < lbp_threshold_factor*lbp_threshold:
                    CASE = 1
                    distance_lbp_list.append(dist_chi_square_hist)
                    lbp_threshold = sum(distance_lbp_list)/ float(len(distance_lbp_list))

                else:
                    CASE = 2
                    distance_lbp_list.clear()
                    # WHAT ABOUT THRESHOLD VALUE- lbp_threshold ???????

                bbox_lbp_reference, overflow_bbbox = crop_image(image,x,y,w,h)
                object_in_video = True


            # elif True: #Test case 3
            #     CASE = 3
            #     TDOT_CHOOSE = False
            #     x_final_corr, y_final_corr, w_final_corr, h_final_corr = 0,0,0,0
            #     final_corr_score = 0.0
            #     final_corr_score_min = 0
            #     tdot_candidate_is = False
            #     object_in_video=False
            #     dist_chi_square_hist= 0
            #     lbp_threshold = 0
            #     distance_lbp_list = []
            #     std_distance_lbp_list =0

            elif tdot_candidate_satisfied== False and object_in_video==True:
                TDOT_CHOOSE = False
                
                # track with correlation filter
                b_box_corr = kcf_tracker.update(image)
                response_corr_pvpr = kcf_tracker.response_p_arr_pvpr
                response_corr_psr = kcf_tracker.response_p_arr_psr
                response_corr_epsr = kcf_tracker.response_p_arr_epsr
                x_final_corr, y_final_corr, w_final_corr,h_final_corr = np.int32(b_box_corr[0]),np.int32(b_box_corr[1]),np.int32(b_box_corr[2]),np.int32(b_box_corr[3])
                


                overflow = are_corners_inimage(image,x_final_corr, y_final_corr, w_final_corr,h_final_corr)

                if overflow != None:
                    CASE = 3
                #    object not found - give coordinates to -1.0
                    object_in_video==False
                    #match_bbox_for_particle_seed = [0.0, 0.0, 0.0, 0.0]
                    match_bbox_for_particle_seed = [b_box_corr[0],b_box_corr[1],b_box_corr[2],b_box_corr[3]]
                    x_final_corr, y_final_corr, w_final_corr, h_final_corr = 0, 0, 0, 0  #FILIZ
                    dist_chi_square_hist = -1.0
                else:

                    # to draw bbox !
                    b_box_image = cv2.rectangle(b_box_image, (int(b_box_corr[0]), int(b_box_corr[1])), (int(b_box_corr[0]+b_box_corr[2]), int(b_box_corr[1]+b_box_corr[3])), (255,255,255), 1)


                    final_corr_score = response_corr_psr[0]
                    final_corr_score_min = -1
                    
                    
                    bbox_lbp_candidate, overflow_bbbox =  crop_image(image,x_final_corr, y_final_corr, w_final_corr,h_final_corr)
                    
                    bbox_lbp_candidate_resize = resize_bbox(bbox_lbp_candidate,bbox_lbp_reference.shape[1],bbox_lbp_reference.shape[0])


                    lbp_hist_reference, bins = lbp_decimal_8(bbox_lbp_reference, hist_conc=True)
                    lbp_hist_condidate, bins = lbp_decimal_8(bbox_lbp_candidate_resize, hist_conc=True)

                    dist_chi_square_hist = chi_square_dist(lbp_hist_reference,lbp_hist_condidate)
                    
                    if dist_chi_square_hist < lbp_threshold_factor_nonfound*lbp_threshold: #Test case 1
                    # if True: #Test case 2
                        CASE = 4
                        distance_lbp_list.append(dist_chi_square_hist)
                        lbp_threshold = sum(distance_lbp_list)/ float(len(distance_lbp_list))
                        match_bbox_for_particle_seed = [b_box_corr[0],b_box_corr[1],b_box_corr[2],b_box_corr[3]]
                        object_in_video = True
                    else:
                        CASE = 5
                        distance_lbp_list.clear()
                        # WHAT ABOUT THRESHOLD VALUE- lbp_threshold ???????
                        #dist_chi_square_hist = -1.0   #FILIZ DEGISTIRDI
                        object_in_video = False
                        x_final_corr, y_final_corr, w_final_corr, h_final_corr = 0, 0, 0, 0
                        match_bbox_for_particle_seed = [0.0, 0.0, 0.0, 0.0]
                

            elif tdot_candidate_satisfied== False and object_in_video==False:
                CASE = 6
                object_in_video==False
                dist_chi_square_hist = -1.0
                x_final_corr, y_final_corr, w_final_corr, h_final_corr = 0, 0, 0, 0 
                match_bbox_for_particle_seed = [0.0, 0.0, 0.0, 0.0]
                

            if not os.path.exists(ARG_OUT_DIR + "/images_"+str(video_name)+"/"):
                os.makedirs(ARG_OUT_DIR + "/images_"+str(video_name)+"/")
            cv2.imwrite(ARG_OUT_DIR + "/images_"+str(video_name)+"/"+image_name+".jpg" ,b_box_image)

            
            # calculating std of distance_lbp_list!!!!!
            np_distance_lbp_list = np.array(distance_lbp_list)
            if np_distance_lbp_list.size >0:
                std_distance_lbp_list = np.std(np_distance_lbp_list)
            else:
                std_distance_lbp_list = -1

                
            if TDOT_CHOOSE:
                with open(ARG_OUT_DIR + "/" +video_name +" correlation output ", 'a+') as f:
                    things_to_write = "{}\t\t\t{}\t{}\t{}\t{}\t\t{}\t{}\t\t{}\t{}\t{}\t{}\t{}\t\t\t\t{}\t\t{}\t{}\n" \
                        .format(image_name[:-4], x, y, w, h, format(final_corr_score, '.4f'),format(final_corr_score_min, '.4f'),"TDOT",1*tdot_candidate_is,CASE, 1*object_in_video,format(dist_chi_square_hist, '.5f'), format(lbp_threshold, '.5f'),len(distance_lbp_list), format(std_distance_lbp_list, '.5f'))
                    
                    f.write(things_to_write)
            
            # r(scores) empty
            else:
                with open(ARG_OUT_DIR + "/" +video_name +" correlation output ", 'a+') as f:
                    things_to_write = "{}\t\t\t{}\t{}\t{}\t{}\t\t{}\t{}\t\t{}\t{}\t{}\t{}\t{}\t\t\t\t{}\t\t{}\t{}\n" \
                        .format(image_name[:-4], x_final_corr, y_final_corr, w_final_corr, h_final_corr, format(final_corr_score, '.4f'),format(final_corr_score_min, '.4f'),"CORR",1*tdot_candidate_is,CASE, 1*object_in_video,format(dist_chi_square_hist, '.5f'), format(lbp_threshold, '.5f'),len(distance_lbp_list), format(std_distance_lbp_list, '.5f'))
                    f.write(things_to_write)

            save_particles = False
            print("\nLL 1 save_particles:",save_particles )
            # region [Particle Sampling] --
            if match_bbox_for_particle_seed is None:
                pass
#                no_result_counter += 1
#                if match_bbox_for_particle_seed_last is None:
#                    # we have no reference so will just rely on vanilla mask-rcnn
#                    particles = np.zeros((1, PARTICLE_COUNT, 4))
#                else:
#                    if no_result_counter < NO_RESULT_RESET_THRESHOLD:
#                        roi_threshold += ROI_THRESHOLD
#                        GAUSS_STDEV_COORD *= STDEV_COORD_MISS_MULTIPLIER
#                        GAUSS_STDEV_SCALE *= STDEV_SCALE_MISS_MULTIPLIER
#                        current_frame_index -= 1
#                    else:
#                        roi_threshold = ROI_THRESHOLD
#                        GAUSS_STDEV_COORD = GAUSS_STDEV_COORD_INIT
#                        GAUSS_STDEV_SCALE = GAUSS_STDEV_SCALE_INIT
#                        save_particles = True
#                    particles = utils.sample_bbox(match_bbox_for_particle_seed_last, stdev_coord=GAUSS_STDEV_COORD,stdev_scale=GAUSS_STDEV_SCALE, count=PARTICLE_COUNT, canvas_shape=canvas_shape_x_y_format)

            else:
                # do stuff
                random.seed(5)
                particles = utils.sample_bbox(match_bbox_for_particle_seed, stdev_coord=GAUSS_STDEV_COORD, stdev_scale=GAUSS_STDEV_SCALE, count=PARTICLE_COUNT, canvas_shape=canvas_shape_x_y_format)
                match_bbox_for_particle_seed_last = match_bbox_for_particle_seed
#                roi_threshold = ROI_THRESHOLD
                GAUSS_STDEV_COORD = GAUSS_STDEV_COORD_INIT
                GAUSS_STDEV_SCALE = GAUSS_STDEV_SCALE_INIT
                save_particles = True

            current_frame_index += 1

