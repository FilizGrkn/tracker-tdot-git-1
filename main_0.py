import numpy as np
from tracker import Tracker
import math


#----------------------------------------TO RUN FOR MULTIPLE VIDEOS -------------------------------------

#directroy of image list
img_dir_list = ["data/ball1_img/",
                "data/ball2_img/",
                "data/basketball_img/",
                "data/birds1_img/",
                "data/car1_img/",
                "data/nature_img/",
                "data/wiper_img/"]

#video names
video_name = ["ball1",
            "ball2",
            "basketball",
            "birds1", 
            "car1", 
            "nature",
            "wiper"]

#initial gt for each video
video_gt = [[496, 419, 40, 42],
          [532, 53, 13, 13],
          [190, 210, 35, 107],
          [508, 233, 83, 18],
          [243, 165, 116, 111],
          [749, 245, 165, 79],
          [287, 253, 41, 41]]

#object to track in corresponding video
video_obj = ["sports ball", 
           "sports ball", 
           "person",
           "bird", 
           "car",
           "bird",
           "car"]

#This loop performs both KCF and SIAM trackers
for i, video_name in enumerate(video_name):
    print(i, video_name, video_obj[i], video_gt[i])
    

#    KCF
    
    tr_kcf = Tracker(img_list_dir= img_dir_list[i], 
                 init_gt_bbox = video_gt[i], 
                 object_name= video_obj[i],
                 auxilary_tracker_type = "KCF", 
                 short_term = True)
    
    
    a_kcf = tr_kcf.bbox_results
    
#    with open( video_name+"_kcf.txt", 'a+') as f:
#        for j in range(a_kcf.shape[0]):
#            res_bbox_frame = "{}\t{}\t{}\t{}\n".format(format(a_kcf[j,0],'.4f'), format(a_kcf[j,1],'.4f'), format(a_kcf[j,2],'.4f'), format(a_kcf[j,3],'.4f'))
#            f.write(res_bbox_frame)
        
#    SIAM    
    tr_siam = Tracker(img_list_dir= img_dir_list[i], 
                 init_gt_bbox = video_gt[i], 
                 object_name= video_obj[i],
                 auxilary_tracker_type = "SIAM", 
                 short_term = True)
    
    a_siam = tr_siam.bbox_results
    
#    with open( video_name+"_siam.txt", 'a+') as f:
#        for j in range(a_kcf.shape[0]):
#            res_bbox_frame = "{}\t{}\t{}\t{}\n".format(format(a_siam[j,0],'.4f'), format(a_siam[j,1],'.4f'), format(a_siam[j,2],'.4f'), format(a_siam[j,3],'.4f'))
#            f.write(res_bbox_frame)

#-------------------------------------TO RUN FOR ONLY ONE VIDEO-------------

img_dir = "data/ball1_img/"

ball1_gt = [496, 419, 40, 42] - "sports ball"


tr = Tracker(img_list_dir= img_dir, 
             init_gt_bbox = ball1_gt, 
             object_name= "sports ball",
             auxilary_tracker_type = "KCF", 
             short_term = True)

#results
bbox = tr.bbox_results

#for each frame whether TDOT os KCF/SIAM has been an active
tracker_name = tr.frame_tacker
#
#to write files
with open( "ball1_kcf.txt", 'a+') as f:
    for i in range(bbox.shape[0]):
        res_bbox_frame = "{}\t{}\t{}\t{}\n".format(format(bbox[i,0],'.4f'), format(bbox[i,1],'.4f'), format(bbox[i,2],'.4f'), format(bbox[i,3],'.4f'))

        f.write(res_bbox_frame)


#--------------------------ADDITIONAL INFORMATION----------------------------------------------

#ball1=sports ball
#ball2=sports ball
#basketball=person
#birds1=bird
#birds2=bird
#blanket=person
#bmx=person
#bolt1=person
#bolt2=person
#book=book
#car1=car
#car2=car
#fernando=cat
#girl=person
#graduate=person
#gymnastics1=person
#gymnastics2=person
#gymnastics3=person
#gymnastics4=person
#handball1=person
#handball2=person
#iceskater1=person
#iceskater2=person
#motocross1=motorcycle
#motocross2=motorcycle
#nature=bird
#pedestrian1=person
#pedestrian2=person
#racing=car
#road=motorcycle
#sheep=sheep
#singer1=person
#singer2=person
#soccer2=person
#traffic=person
#tunnel=truck
#wiper=car


#class VotGt2016Bbox:
#    #def __init__(self, top_left_x, top_left_y, top_right_x, top_right_y, bottom_right_x, bottom_right_y, bottom_left_x, bottom_left_y):
#    def __init__(self, tokens):
#        # self.top_left_x = top_left_x
#        # self.top_left_y = top_left_y
#        # self.top_right_x = top_right_x
#        # self.top_right_y = top_right_y
#        # self.bottom_right_x = bottom_right_x
#        # self.bottom_right_y = bottom_right_y
#        # self.bottom_left_x = bottom_left_x
#        # self.bottom_left_y = bottom_left_y
#
#        self.top_left_x = float(tokens[0])
#        self.top_left_y = float(tokens[1])
#        self.top_right_x = float(tokens[2])
#        self.top_right_y = float(tokens[3])
#        self.bottom_right_x = float(tokens[4])
#        self.bottom_right_y = float(tokens[5])
#        self.bottom_left_x = float(tokens[6])
#        self.bottom_left_y = float(tokens[7])
#
#    def convert_to_mrcnn_bbox(self):
#        """
#        (1)----(2)
#        \       \
#        \       \
#        (4)----(3)
#
#        From format
#            x1,y1,x2,y2,x3,y3,x4,y4
#        to format
#            x1,y1,w,h
#        :return:
#        """
#        left_avg = math.floor((self.top_left_x + self.bottom_left_x) / 2)
#        top_avg = math.floor((self.top_left_y + self.top_right_y) / 2)
#        width_avg = math.floor(((self.top_right_x - self.top_left_x)+(self.bottom_right_x - self.bottom_left_x))/2)
#        height_avg = math.floor(((self.bottom_left_y - self.top_left_y) + (self.bottom_right_y - self.top_right_y)) / 2)
#        return [left_avg, top_avg, width_avg, height_avg]
#
#def get_initial_ground_truth():
#    with open("data/ball1_gt/groundtruth.txt",'r') as f:
#        line = f.readline()
#        tokens = line.split(',')
#        tokens = list(filter(None, tokens))  # filter whitespace
#        gt_bbox = VotGt2016Bbox(tokens).convert_to_mrcnn_bbox()
#    return gt_bbox # x,y,w,h
#
#gt = get_initial_ground_truth()
#print(gt)        

